# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  capital_str = ""

  (0..(str.length - 1)).each do |i|
    capital_str << str[i] if str[i] == str[i].upcase
  end

  capital_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    str[str.length / 2]
  else
    str[str.length / 2 - 1] + str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  counter = 0
  (0..(str.length - 1)).each do |i|
    counter += 1 if VOWELS.include?(str[i])
  end

  counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  # (1..num).inject(:*)
  (1..num).inject(1) { |product, number| product * number }
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return '' if arr.empty?
  join_arr = ""

  (0..(arr.count - 1)).each do |i|
    if i != arr.count - 1
      join_arr += "#{arr[i]}#{separator}"
    else
      join_arr += "#{arr[i]}"
    end
  end

    join_arr
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = ""

  (0..(str.length - 1)).each do |i|
    if i % 2 != 0
      weird_str += str[i].upcase
    else
      weird_str += str[i].downcase
    end
  end

  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reversed_str = []

  str.split(" ").each do |word|
    if word.length >= 5
      reversed_str << word.reverse
    else
      reversed_str << word
    end
  end

  reversed_str.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []

  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      result << "fizzbuzz"
    elsif num % 3 == 0
      result << "fizz"
    elsif num % 5 == 0
      result << "buzz"
    else
      result << num
    end
  end

  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2..(num - 1)).each { |number| return false if num % number == 0}

  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = (1..num).select { |number| number if num % number == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |number| number if prime?(number) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_nums = arr.select { |num| num if num.odd? }
  even_nums = arr.select { |num| num if num.even? }
  odd_nums.count > even_nums.count ? even_nums[0] : odd_nums[0]
end
